import { Component } from "react";
import { connect } from "react-redux";
import "./navbar.css";
import {save} from "../application/services/save";

interface NavProps{
    title: string,
}

class Nav extends Component<NavProps>{
    render() {
        return (
            <div className="nav">
                <p>{this.props.title}</p>
            </div>
        )
    }
}

interface NavBarProps{
    switchToCurrentEntry:any
    switchToAllEntries:any
    save:any
    children:any
    currentPage: number
}

class Navbar extends Component<NavBarProps>{
    render() {
        return (
            <div className="navbar">
                <div onClick={this.props.switchToCurrentEntry}><Nav title="Current Entry"></Nav></div>
                <div onClick={this.props.switchToAllEntries}><Nav title="All Entries"></Nav></div>
                <div onClick={this.props.save}><Nav title="Save"></Nav></div>
            </div>
        )
    }
}

function mapStateToProps(state:any){
    return {
        currentPage: state.currentPage
    }
}

function mapDispatchToProps(dispatch: Function){
    return {
        switchToCurrentEntry: ()=>dispatch({type:"switch_page", newPage: 1}),
        switchToAllEntries: ()=>dispatch({type:"switch_page", newPage: 2}),
        save: ()=>save()
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navbar)