import {Component} from "react";
import Navbar from "./navbar";
import "./index.css"
import {connect} from "react-redux";
import Entry from "./entry";
import AllEntrys from "./allEntries"

interface Props {
    currentPage: number,
}

class Index extends Component<Props>{
    render() {
        return (
            <div className="page">
                <Navbar>
                    
                </Navbar>
                {this.props.currentPage ===1? <Entry></Entry>:null}
                {this.props.currentPage ===2? <AllEntrys></AllEntrys>:null}
            </div>
        )
    }
}



function mapStateToProps(state:{[key:string]:any}){
    return {
        currentPage: state.NavbarReducer.currentPage
    }
}


export default connect(
    mapStateToProps,
    (dispatch:Function)=>{return {}}
)(Index)