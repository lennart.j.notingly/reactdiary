import {Component} from "react";
import "./allEntries.css";
import {connect} from "react-redux";

interface EntryProps{
    content: string,
}

class Entry extends Component<EntryProps>{
    render() {
        return (
            <div className="entry">
                <p>{this.props.content}</p>
            </div>
        )
    }
}

interface AllEntriesProps {
    switchEntry: Function
    switchToCurrentEntry:Function
    content:{[key:string]:any}
    createEntry:Function
}

class AllEntries extends Component<AllEntriesProps>{
    render() {
            return (
            <div className="allEntries">
                <div onClick={()=>{this.props.createEntry()}}>
                    <Entry content={"New Entry"}>
                        
                    </Entry>
                </div>
                {Object.keys(this.props.content.content).map((contentID)=>{
                    return (
                        <div onClick={()=>{this.props.switchEntry(contentID)}} key={contentID}>
                            <Entry content = {this.props.content.content[contentID]} key={contentID}>

                            </Entry>
                    </div>
                    )
                })}
            </div>
        )
    }
}

function mapStateToProps(state:{[key:string]:any}){
    return {
        content: state.EntryReducer
    }
}

function mapDispatchToProps(dispatch:Function){
    return {
        switchEntry : (newEntryId:string) =>{dispatch({type:"update_id",id:newEntryId})},
        switchToCurrentEntry: ()=>dispatch({type:"switch_page", newPage: 1}),
        createEntry: ()=> dispatch({type:"new_entry"})
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AllEntries)