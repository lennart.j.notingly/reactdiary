import { Component } from "react";
import { connect } from "react-redux";
import "./entry.css"

interface EntryProps{
    content:string
    editEntry:Function
}

class Entry extends Component<EntryProps>{
    render() {
        return (
            <div className = "entry">
                <textarea value = {this.props.content} onChange={(event)=>{
                    this.props.editEntry(event.target.value)
                }} rows = {32} cols = {80}></textarea>
            </div>
        )
    }
}

function mapStateToProps(state:{[key:string]:any}){
    return {
        content: state.EntryReducer.content[state.EntryReducer.currentEntryId]
    }
}


function mapDispatchToProps(dispatch:Function){
    return {
        editEntry: (content:string)=>dispatch({type:"update_entry",content:content})
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Entry)