const initialState = {
    currentPage: 2
};
type Action = {
    newPage: number
    type: string
}

const reducer = (state = initialState, action:Action) => {
    switch (action.type) {
        case "switch_page":
            return {...state, currentPage: action.newPage};
        default:
            return state;
    }
}

export default reducer;