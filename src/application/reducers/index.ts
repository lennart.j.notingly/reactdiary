import { combineReducers } from 'redux';
import NavbarReducer from './navbar';
import EntryReducer from './entry';

export default combineReducers({
    NavbarReducer,
    EntryReducer,
})