const initialState:{content: {[key:string]:string|undefined},currentEntryId: string} = {
    content: {"1":"Lorem Ipsum"},
    currentEntryId: "1"
};
type Action = {
    type: string
    content?: string
    id?: string
};

const reducer = (state = initialState, action:Action) => {
    let newContent = {...state.content};
    switch (action.type) {
        case "update_entry":
            newContent[state.currentEntryId] = action.content;
            return {...state, content:newContent};
        case "update_id":
            return {...state, currentEntryId:action.id};
        case "new_entry":
            let id = Math.random().toString()
            newContent[id] = "";
            return {...state, currentEntryId:id,content:newContent};
        case "load_content":
            let DumpedData = localStorage.getItem("content");
            if(DumpedData!==null){
                return {...state,content: JSON.parse(DumpedData)}
            }
            return {...state}
        default:
            return state;
    }
};

export default reducer;