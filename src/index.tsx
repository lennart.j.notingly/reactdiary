import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import store from "./application/store"
import Index from "./views/index"

store.dispatch({type:"load_content"});

ReactDOM.render(
  <Provider store={store}>
    <Index></Index>
  </Provider>,
  document.getElementById('root')
);
